#!/bin/bash

#                           PyVTCheck
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: install-unix.sh                                        #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 05-April-2021                                          #
#   LAST MODIFIED: 05-April-2021                                          #
###########################################################################

# Cleanup
echo ""
echo "******************* PyVTCheck Unix Install Script v0.1.0 *******************"
echo ""
echo -n "Cleaning up old versions of PyVTCheck if present..."
if [ -L "$HOME/.local/bin/pyvtcheck" ]; then
    unlink "$HOME/.local/bin/pyvtcheck"
fi
if [ -d "$HOME/.local/scripts/pyvtcheck/shell" ]; then
    rm -rf "$HOME/.local/scripts/pyvtcheck/shell"
fi
if [ -f "$HOME/.local/scripts/pyvtcheck/LICENCE" ]; then
    rm -rf "$HOME/.local/scripts/pyvtcheck/LICENCE"
fi
if [ -f "$HOME/.local/scripts/pyvtcheck/README.md" ]; then
    rm -rf "$HOME/.local/scripts/pyvtcheck/README.md"
fi
if [ -f "$HOME/.local/scripts/pyvtcheck/pyvtcheck.py" ]; then
    rm -rf "$HOME/.local/scripts/pyvtcheck/pyvtcheck.py"
fi
echo "Done"
# Make directories
echo -n "Creating PyVTCheck directories..."
if [ ! -d "$HOME/.local/scripts/pyvtcheck/etc" ]; then
    mkdir -p "$HOME/.local/scripts/pyvtcheck/etc"
fi
if [ ! -d "$HOME/.local/scripts/pyvtcheck/shell" ]; then
    mkdir -p "$HOME/.local/scripts/pyvtcheck/shell"
fi
echo "Done"
# Copy files/folders
echo -n "Copying required files/folders..."
ROOTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
cp -r "$ROOTPATH/shell" "$HOME/.local/scripts/pyvtcheck"
cp "$ROOTPATH/LICENCE" "$HOME/.local/scripts/pyvtcheck"
cp "$ROOTPATH/README.md" "$HOME/.local/scripts/pyvtcheck"
cp "$ROOTPATH/pyvtcheck.py" "$HOME/.local/scripts/pyvtcheck"
echo "Done"
# Make shell script executable
chmod +x "$HOME/.local/scripts/pyvtcheck/shell/pyvtcheck.sh"
# Create symbolic link
echo -n "Creating symbolic link in $HOME/.local/bin (make sure this is on your PATH)..."
ln -s "$HOME/.local/scripts/pyvtcheck/shell/pyvtcheck.sh" "$HOME/.local/bin/pyvtcheck"
echo "Done"
# Show success message
echo ""
echo "***************************************************************************"
echo "*                                                                         *"
echo "*                     PyVTCheck installed successfully                    *"
echo "*                                                                         *"
echo "***************************************************************************"
echo ""
