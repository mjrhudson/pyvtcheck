@echo off

REM                           PyVTCheck
REM               Copyright (C) 2021  Matt Hudson
REM                   Email: matt@mjrhudson.uk

REM This program is free software: you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or
REM (at your option) any later version.

REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.

REM You should have received a copy of the GNU General Public License
REM along with this program.  If not, see <http://www.gnu.org/licenses/>.

REM #######################################################################
REM #           TITLE: install-win-contextmenu.bat                        #
REM #         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                    #
REM # INITIAL RELEASE: 05-April-2021                                      #
REM #   LAST MODIFIED: 05-April-2021                                      #
REM #######################################################################

REM Elevate script
if not "%1"=="elevated" (powershell start -Verb RunAs "%0" "elevated" & exit /B)

REM Display header
echo.
echo *************** PyVTCheck Windows Context Menu Script v0.1.0 **************
echo.

REM Create key command definition
echo|set /p="Creating command definition..."
REG ADD "HKEY_CLASSES_ROOT\*\shell\PyVTCheck" /f /ve /t REG_SZ /d "Run PyVTCheck" > nul 2>&1
echo Done

REM Create subkey and value for command
echo|set /p="Creating command..."
REG ADD "HKEY_CLASSES_ROOT\*\shell\PyVTCheck\command" /f /ve /t REG_SZ /d "\"%USERPROFILE%\.local\scripts\pyvtcheck\shell\pyvtcheck.bat\" \"%%1\"" > nul 2>&1
echo Done

REM Show success message
echo.
echo ***************************************************************************
echo *                                                                         *
echo *             PyVTCheck context menu entry created successfully           *
echo *                                                                         *
echo ***************************************************************************
echo.
pause
echo.
