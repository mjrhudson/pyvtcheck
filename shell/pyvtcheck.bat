@echo off

REM                           PyVTCheck
REM               Copyright (C) 2021  Matt Hudson
REM                   Email: matt@mjrhudson.uk

REM This program is free software: you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or
REM (at your option) any later version.

REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.

REM You should have received a copy of the GNU General Public License
REM along with this program.  If not, see <http://www.gnu.org/licenses/>.

REM #######################################################################
REM #           TITLE: pyvtcheck.py                                       #
REM #         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                    #
REM # INITIAL RELEASE: 05-April-2021                                      #
REM #   LAST MODIFIED: 05-April-2021                                      #
REM #######################################################################

REM Place this shell script in any location for executables on
REM the PATH, e.g ~/.local/bin

REM Place the pyvtcheck root directory in ~/.local/scripts/pyvtcheck (or
REM any location of your choice where you have write permissions
REM - just be sure to modify pyvtcheckpath below)

set pyvtcheckpath="%USERPROFILE%\.local\scripts\pyvtcheck\pyvtcheck.py"
python -B "%pyvtcheckpath%" %*
pause
echo.
