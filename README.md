# PyVTCheck - Python VirusTotal Checking Tool

Command line based tool written in Python for checking a file signature with the VirusTotal API.

## Requirements

PyVTCheck has been written with cross-platform compatibility in mind, however currently the context menu functionality is only supported on Windows.

The following must be installed and on your system PATH:

- Python v3.6+

In addition to this, you must have a valid API key for VirusTotal. You can get a free, limited-use API key by signing up to the VirusTotal community: https://www.virustotal.com/gui/join-us

## Issues

Please submit any issues to the issues tracker, I will do my best to keep on top of them where possible.
