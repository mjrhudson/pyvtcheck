@echo off

REM                           PyVTCheck
REM               Copyright (C) 2021  Matt Hudson
REM                   Email: matt@mjrhudson.uk

REM This program is free software: you can redistribute it and/or modify
REM it under the terms of the GNU General Public License as published by
REM the Free Software Foundation, either version 3 of the License, or
REM (at your option) any later version.

REM This program is distributed in the hope that it will be useful,
REM but WITHOUT ANY WARRANTY; without even the implied warranty of
REM MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
REM GNU General Public License for more details.

REM You should have received a copy of the GNU General Public License
REM along with this program.  If not, see <http://www.gnu.org/licenses/>.

REM #######################################################################
REM #           TITLE: install-win.bat                                    #
REM #         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                    #
REM # INITIAL RELEASE: 05-April-2021                                      #
REM #   LAST MODIFIED: 05-April-2021                                      #
REM #######################################################################

REM Cleanup
echo.
echo ****************** PyVTCheck Windows Install Script v0.1.0 ****************
echo.
echo|set /p="Cleaning up old versions of PyVTCheck if present..."
if exist "%USERPROFILE%\.local\bin\pyvtcheck.bat" del "%USERPROFILE%\.local\bin\pyvtcheck.bat"
if exist "%USERPROFILE%\.local\scripts\pyvtcheck\shell" rmdir /s /q "%USERPROFILE%\.local\scripts\pyvtcheck\shell"
if exist "%USERPROFILE%\.local\scripts\pyvtcheck\LICENCE" del "%USERPROFILE%\.local\scripts\pyvtcheck\LICENCE"
if exist "%USERPROFILE%\.local\scripts\pyvtcheck\README.md" del "%USERPROFILE%\.local\scripts\pyvtcheck\README.md"
if exist "%USERPROFILE%\.local\scripts\pyvtcheck\pyvtcheck.py" del "%USERPROFILE%\.local\scripts\pyvtcheck\pyvtcheck.py"
echo Done
REM Make directories
echo|set /p="Creating PyVTCheck directories..."
if not exist "%USERPROFILE%\.local\bin" mkdir "%USERPROFILE%\.local\bin"
if not exist "%USERPROFILE%\.local\scripts\pyvtcheck\etc" mkdir "%USERPROFILE%\.local\scripts\pyvtcheck\etc"
if not exist "%USERPROFILE%\.local\scripts\pyvtcheck\shell" mkdir "%USERPROFILE%\.local\scripts\pyvtcheck\shell"
echo Done
REM Copy files/folders
echo|set /p="Copying required files/folders..."
xcopy /s /y /i "%~dp0shell" "%USERPROFILE%\.local\scripts\pyvtcheck\shell" 1>NUL
echo f | xcopy /s /y /f "%~dp0LICENCE" "%USERPROFILE%\.local\scripts\pyvtcheck\LICENCE" 1>NUL
echo f | xcopy /s /y /f "%~dp0README.md" "%USERPROFILE%\.local\scripts\pyvtcheck\README.md" 1>NUL
echo f | xcopy /s /y /f "%~dp0pyvtcheck.py" "%USERPROFILE%\.local\scripts\pyvtcheck\pyvtcheck.py" 1>NUL
echo Done
REM Create call script
echo|set /p="Creating call script in %USERPROFILE%\.local\bin (make sure this is on your PATH)..."
echo ^@echo off > "%USERPROFILE%\.local\bin\pyvtcheck.bat"
echo call %USERPROFILE%\.local\scripts\pyvtcheck\shell\pyvtcheck.bat %%* >> "%USERPROFILE%\.local\bin\pyvtcheck.bat"
echo Done
REM Show success message
echo.
echo ***************************************************************************
echo *                                                                         *
echo *                     PyVTCheck installed successfully                    *
echo *                                                                         *
echo ***************************************************************************
echo.
pause
echo.
