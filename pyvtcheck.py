#                           PyVTCheck
#               Copyright (C) 2021  Matt Hudson
#                   Email: matt@mjrhudson.uk

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

###########################################################################
#           TITLE: pyvtcheck.py                                           #
#         AUTHORS: Matt Hudson (matt@mjrhudson.uk)                        #
# INITIAL RELEASE: 05-April-2021                                          #
#   LAST MODIFIED: 05-April-2021                                          #
###########################################################################

# SECTION Import Libraries
import sys
import os
import hashlib
import requests
import json
import subprocess
import pathlib
# !SECTION Import Libraries

# SECTION Define Functions
def loadApikey(filepath):
    with open(filepath,"r") as f:
        apikey = json.load(f)["apikey"]
    return apikey

def sha1file(filepath):
    sha1 = hashlib.sha1()
    with open(filepath,"rb") as f:
        while True:
            chunk = f.read(65536)
            if not chunk:
                break
            sha1.update(chunk)
    return sha1.hexdigest()

def getFileInfo(filepath,apiurl,apikey):
    sha1 = sha1file(filepath)
    url = apiurl + sha1
    query = {"x-apikey": apikey}
    response = requests.get(url,headers=query)
    if not (response.status_code == 200 or response.status_code == 404 or response.status_code == 401):
        fileInfo = {"error": "Response status code " + str(response.status_code)}
    else:
        responseJson = json.loads(response.text)
        if "error" in responseJson.keys():
            fileInfo = {"error": responseJson["error"]["message"]}
        else:
            data = responseJson["data"]
            stats = data["attributes"]["last_analysis_stats"]
            fileInfo = {"stats": stats}
    return fileInfo

def printFileInfo(filepath,fileInfo):
    name = os.path.split(filepath)[-1]
    if "error" in fileInfo.keys():
        verdict = "Error"
    elif (fileInfo["stats"]["suspicious"] + fileInfo["stats"]["malicious"]) == 0:
        verdict = "Safe"
    elif (fileInfo["stats"]["suspicious"] + fileInfo["stats"]["malicious"]) <= 5:
        verdict = "Requires attention"
    elif (fileInfo["stats"]["suspicious"] + fileInfo["stats"]["malicious"]) > 5:
        verdict = "Potentially unsafe"
    print("***************************************************************************",end="\n\n")
    print("Name: " + name,end="\n\n")
    print("Verdict: " + verdict,end="\n\n")
    if "error" in fileInfo.keys():
        print("Error message: " + fileInfo["error"])
    else:
        print("    Undetected: " + str(fileInfo["stats"]["undetected"]))
        print("    Harmless:   " + str(fileInfo["stats"]["harmless"]))
        print("    Suspicious: " + str(fileInfo["stats"]["suspicious"]))
        print("    Malicious:  " + str(fileInfo["stats"]["malicious"]))
    print("\n***************************************************************************",end="\n\n")
# !SECTION Define Functions

# SECTION Main
filepath = sys.argv[1]
etcPath = str(pathlib.Path(__file__).parent.absolute()) + os.path.sep + "etc"
apiurl = "https://www.virustotal.com/api/v3/files/"
apikeyFilepath = etcPath + os.path.sep + "apikey.json"

if not os.path.isfile(filepath):
    print("Error: Invalid filepath provided\n")
    exit()

if not os.path.isfile(apikeyFilepath):
    with open(apikeyFilepath,"w") as f:
        f.write("{\n    \"apikey\": \"yourapikeyhere\"\n}\n")
    print("Error: No API key file found\n\nA template file has been created in \"" + etcPath + "\" for you to enter your own API key\n")
    exit()

apikey = loadApikey(apikeyFilepath)
fileInfo = getFileInfo(filepath,apiurl,apikey)
printFileInfo(filepath,fileInfo)
# !SECTION Main
